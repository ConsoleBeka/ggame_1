﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForGame
{
    class Program
    {
        static void Main(string[] args)
        {
            string ans = "";
            bool answer=false;
            int points = 0;
            using (StreamReader r = new StreamReader("Questions.json"))
            {
                
                List<Questions> questions = new List<Questions>();
                string json = r.ReadToEnd();
                
                questions = JsonConvert.DeserializeObject<List<Questions>>(json);
                
                Console.WriteLine(questions);
                Console.WriteLine("1 for true");
                Console.WriteLine("2 for false");

                foreach (var item in questions)
                {
                    Console.WriteLine(item.name_ru);
                    ans=Console.ReadLine();
                    if (ans == "1")
                    {
                        answer = true;
                       
                    }
                    else if (ans == "2")
                    {
                        answer = false;
                    }
                    if (answer == item.right_answer)
                    {
                        Console.WriteLine("Correct!");
                        points++;
                    }
                    else
                    {
                        Console.WriteLine("Wrong!");
                        break;
                    }
                }
                Console.WriteLine("Points " + points);

            }
            Console.Read();
        }
    }
}
