﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ForGame
{
    public class Questions
    {
        public int id { get; set; }
        public string name_ru { get; set; }
        public bool right_answer { get; set; }
    }
}